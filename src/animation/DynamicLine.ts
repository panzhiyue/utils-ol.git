import { Style, Stroke } from "ol/style"
import Feature from "ol/Feature"
import LineString from "ol/geom/LineString";

export interface DynamicLineOptions {
    feature: Feature<LineString>;
    dashOffset: number;
}

/**
 * 动态线
 */
class DynamicLine {
    /**
     * 道路要素
     */
    feature: Feature<LineString>;

    /**
     * 虚线偏移
     */
    dashOffset: number = 5;

    /**
     * 计时器
     */
    timer = null;

    /**
     * 构造函数
     * @param opt_options
     * @param opt_options.feature 路线
     * @param opt_options.dashOffset 虚线偏移
     */
    constructor(opt_options: DynamicLineOptions) {
        let options = Object.assign({ feature: null, dashOffset: 5 }, opt_options);

        this.feature = options.feature;
        this.dashOffset = options.dashOffset;
    }

    /**
     * 开始动画
     */
    start() {
        var outlineStroke = new Style({
            stroke: new Stroke({
                color: [255, 0, 0, 1],
                width: 8,
            }),
        });

        const getAnimationStrokeStyle = () => {
            return new Style({
                stroke: new Stroke({
                    color: [255, 255, 255, 1],
                    width: 6,
                    lineDash: [0, 12],
                    lineDashOffset: this.dashOffset,
                }),
            });
        }

        const getStyle = () => {
            return [outlineStroke, getAnimationStrokeStyle()];
        }

        this.feature.setStyle(getStyle());

        this.timer = setInterval(() => {
            let offset1 = this.dashOffset;
            offset1 = offset1 == 0 ? -8 : offset1 + 1;

            this.dashOffset = offset1;
            this.feature.setStyle(getStyle());
        }, 150);
    }

    /**
     * 结束动画
     */
    stop() {
        if (this.timer) {
            clearInterval(this.timer);
            this.timer = null;
        }
    }
}

export default DynamicLine;